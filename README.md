## Terminologie MII KDS Basismodul Laborbefund

Dieses Paket enthält die Terminologie aus dem Basismodul Laborbefund des Kerndatensatzes der MII.

Diese Ressourcen wurden umpaketiert aus dem Ursprungspaket: https://simplifier.net/packages/de.medizininformatikinitiative.kerndatensatz.laborbefund/2025.0.2.

---

Dieses Repo wurde am 8.1.2024 verschoben von https://gitlab.com/mii-termserv/fhir-resources/mii-kerndatensatz/de.medizininformatikinitiative.kerndatensatz.terminology.labor nach https://gitlab.com/mii-termserv/fhir-resources/mii-kerndatensatz/de.medizininformatikinitiative.kerndatensatz.terminology.laborbefund, um die Konsistenz mit den Upstream-Paketen zu gewährleisten. Mit dem 2025-Release wurde das Paket ebenfalls umbenannt, von `@mii-termserv/de.medizininformatikinitiative.kerndatensatz.terminology.labor` zu `@mii-termserv/de.medizininformatikinitiative.kerndatensatz.terminology.laborbefund`.

---

Sobald neue Versionen veröffentlicht werden, werden diese auf anderen Git-Branches verfügbar gemacht. Sie sehen aktuell den Stand vom Branch **2025**.

Weitere Branches:
- [1.0.6](https://gitlab.com/mii-termserv/fhir-resources/mii-kerndatensatz/de.medizininformatikinitiative.kerndatensatz.terminology.laborbefund/-/tree/1.0.6)
